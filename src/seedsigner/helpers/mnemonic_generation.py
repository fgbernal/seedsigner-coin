from embit import bip39
from embit.bip39 import mnemonic_to_bytes, mnemonic_from_bytes
import cv2 as cv
import numpy as np

def calculate_checksum(partial_mnemonic: list):
    # Provide 11- or 23-word mnemonic, returns complete mnemonic w/checksum
    if len(partial_mnemonic) not in [11, 23]:
        raise Exception("Pass in a 11- or 23-word mnemonic")

    # Work on a copy of the input list
    mnemonic_copy = partial_mnemonic.copy()
    mnemonic_copy.append("abandon")

    # Ignores the final checksum word and recalcs
    mnemonic_bytes = bip39.mnemonic_to_bytes(" ".join(mnemonic_copy), ignore_checksum=True)

    # Return as a list
    return bip39.mnemonic_from_bytes(mnemonic_bytes).split()



def generate_mnemonic_from_bytes(entropy_bytes):
    # Return as a list
    return bip39.mnemonic_from_bytes(entropy_bytes).split()



def generate_mnemonic_from_dice(roll_data: str):
    entropyinteger = int(roll_data, 6)
    entropy_bytes = entropyinteger.to_bytes(32, byteorder="little")

    # Return as a list
    return bip39.mnemonic_from_bytes(entropy_bytes).split()



# Note: This currently isn't being used since we're now chaining hashed bytes for the
#   image-based entropy and aren't just ingesting a single image.
def generate_mnemonic_from_image(image):
    hash = hashlib.sha256(image.tobytes())

    # Return as a list
    return bip39.mnemonic_from_bytes(hash.digest()).split()

def generate_mnemonic_from_coin_seed_template(image):
    
    def binaryToDecimal(val): 
        return int(val, 2) 
    
    #Return a list with all rectangles, sorted by area
    def rectContour(contours):
        rectCont = []
        for i in contours:
            area = cv.contourArea(i)

            if area > 50:
                peri = cv.arcLength(i,True)
                aprox = cv.approxPolyDP(i, 0.02 * peri, True)
        
                if len(aprox) == 4:
                    rectCont.append(i)    
        rectCon = sorted(rectCont, key=cv.contourArea,reverse=True)      
        return rectCon
    
    def getCornerPoints(cont):
        peri = cv.arcLength(cont,True)
        approx = cv.approxPolyDP(cont, 0.02 * peri, True)
        return approx
    
    def reorder(myPoints):
        myPoints = myPoints.reshape((4, 2))
        myPointsNew = np.zeros((4,1,2), np.int32)
        add = myPoints.sum(1)
        myPointsNew[0] = myPoints[np.argmin(add)] # [0, 0]
        myPointsNew[3] = myPoints[np.argmax(add)] # [w, h]
        diff = np.diff(myPoints, axis=1)
        myPointsNew[1] = myPoints[np.argmin(diff)] # [w, 0]
        myPointsNew[2] = myPoints[np.argmax(diff)] # [h, 0]
        return(myPointsNew)
    
    #split the image in 12 rows and 11 columns of boxes
    def splitBoxes(img):
        rows = np.array_split(img, 12)
        boxes = []
        for r in rows:
            cols = np.array_split(r, 11, axis=1)
            for box in cols:
                boxes.append(box)
        return(boxes)
    
    WIDTH_IMG = 480
    HEIGHT_IMG = 480
    THRESHOLD = 200

    #Preprocessing
    img = np.array(image)
    imgSize = np.shape(img)
    #cv.imwrite("inicial.jpg", np.array(image))
    #img = cv.resize(img, (WIDTH_IMG, HEIGHT_IMG))

    imgContours = img.copy()
    imgGray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

    imgBlur = cv.GaussianBlur(imgGray, (5,5),1)
    imgCanny = cv.Canny(imgBlur, 10, 50)

    #find shapes
    contours, hierarchy = cv.findContours(imgCanny, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)
    
    #find rectangles. Our seed will be the biggest rectangle
    rectCon = rectContour(contours)
    try:
        seed = getCornerPoints(rectCon[0])                
    except Exception as error:
        print('Template not detected: ' + repr(error)) # debug
        return ""
 
    #check if rectangle has been detected
    if seed.size != 0:
        cv.drawContours(imgContours, seed, -1, (0,255,0), 20)
        seed = reorder(seed)

        pt1 = np.float32(seed)
        pt2 = np.float32([[0,0], [WIDTH_IMG, 0], [0, HEIGHT_IMG], [WIDTH_IMG, HEIGHT_IMG]])
        matrix = cv.getPerspectiveTransform(pt1, pt2)
        imgWarpColored = cv.warpPerspective(img, matrix, (WIDTH_IMG, HEIGHT_IMG))
      
        #apply threhsold
        imgWarpGrey = cv.cvtColor(imgWarpColored, cv.COLOR_BGR2GRAY)
        blockSize = int(1 / 8 * imgSize[0] / 2 * 2 + 1)
        if blockSize <= 1:
            blockSize = imgSize[0] / 2 * 2 + 1
        #imgTresh = cv.threshold(imgWarpGrey, 150, 255, cv.THRESH_BINARY_INV)[1]
        imgTresh = cv.adaptiveThreshold(imgWarpGrey,255,cv.ADAPTIVE_THRESH_MEAN_C, cv.THRESH_BINARY_INV,blockSize, 10)
        #remove 7 pixels from borders
        imgTresh = imgTresh[7:HEIGHT_IMG-7, 7:WIDTH_IMG-7]
      
        BOX_WIDTH = int((WIDTH_IMG-14) / 11)
        BOX_HEIGHT = int((HEIGHT_IMG-14) /12)

        boxes = splitBoxes(imgTresh)
      
        myPixelValues = np.zeros((12,11))
        myPixelValues_test = np.zeros((12,11))
        countCol = 0
        countRow = 0
      
        for img in boxes:
            totalPixels = cv.countNonZero(img[3:BOX_HEIGHT-3, 3:BOX_WIDTH-3])
            if totalPixels > THRESHOLD:
                resul = 1
            else:
                resul = 0
            myPixelValues[countRow][countCol] = resul
            myPixelValues_test[countRow][countCol] = int(totalPixels)
            countCol += 1
            if countCol == 11:
                countRow += 1
                countCol = 0     

        strArray = myPixelValues.astype(int).astype(str)
      
        mnemonic_copy = []
        for row in strArray:
            number = ''.join(row)
            #seed before recalculating last word
            mnemonic_copy.append(bip39.WORDLIST[binaryToDecimal(number)])
        mnemonic_bytes = bip39.mnemonic_to_bytes(" ".join(mnemonic_copy), ignore_checksum=True)
        #final seed after recalculating final word
        final_seed = bip39.mnemonic_from_bytes(mnemonic_bytes).split()
    
        #debug
        #print(strArray)
        #print(myPixelValues_test)
        #print(final_seed)
        #cv.imwrite("final.jpg",imgTresh)
        ###
    
    return final_seed